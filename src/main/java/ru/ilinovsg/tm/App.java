package ru.ilinovsg.tm;

import ru.ilinovsg.tm.controller.ProjectController;
import ru.ilinovsg.tm.controller.SystemController;
import ru.ilinovsg.tm.controller.TaskController;
import ru.ilinovsg.tm.controller.UserController;
import ru.ilinovsg.tm.enumerated.Role;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.*;
import ru.ilinovsg.tm.utils.hashMD5;

import java.util.*;

import static ru.ilinovsg.tm.constant.TerminalConst.*;

public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);

    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserProjectTaskService userProjectTaskService = new UserProjectTaskService(userRepository, projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService, userProjectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userProjectTaskService);

    private final UserController userController = new UserController(userService, userProjectTaskService);

    private final SystemController systemController = new SystemController();

    /*private static final Deque<String> commandList = new ArrayDeque<>(10);*/
    private static final PriorityQueue<String> commandList = new PriorityQueue<>(10);

    {
        projectRepository.create("PROJECT 3");
        projectRepository.create("PROJECT 1");
        projectRepository.create("PROJECT 2");
        taskRepository.create("TASK 3");
        taskRepository.create("TASK 1");
        taskRepository.create("TASK 2");

        userRepository.create("ADMIN", hashMD5.md5("123QWE"), "Ivan", "Ivanov", Role.Admin);
        userRepository.create("TEST", hashMD5.md5("123"), "Peter", "Petrov", Role.User);
    }

    public static void main(final String[] args) throws ProjectNotFoundException, TaskNotFoundException {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                app.run(command);
            }
            catch (ProjectNotFoundException | TaskNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void run(final String[] args) throws ProjectNotFoundException, TaskNotFoundException {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws ProjectNotFoundException, TaskNotFoundException {
        if (!param.equals(HISTORY_COMMAND)) {
            if (commandList.size() == 10) {
                commandList.poll();
            }
            commandList.offer(param);
        }
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();
            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_NAME:
                return projectController.updateProjectByName();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME:
                return projectController.viewProjectByName();
            case PROJECT_LIST_BY_USER_ID:
                return projectController.listProjectByUserId();
            case PROJECT_ADD_TO_USER_BY_IDS:
                return projectController.addProjectToUserByIds();
            case PROJECT_REMOVE_FROM_USER_BY_IDS:
                return projectController.removeProjectFromUserByIds();

            case TASK_CREATE:
                return taskController.createTask();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_NAME:
                return taskController.updateTaskByName();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_NAME:
                return taskController.viewTaskByName();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();
            case TASK_REMOVE_WITH_PROJECT_BY_ID:
                return taskController.removeTasksAndProject();
            case TASK_LIST_BY_USER_ID:
                return taskController.listTaskByUserId();
            case TASK_ADD_TO_USER_BY_IDS:
                return taskController.addTaskToUserByIds();
            case TASK_REMOVE_FROM_USER_BY_IDS:
                return taskController.removeTaskFromUserByIds();

            case USER_CREATE:
                return userController.createUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_LIST:
                return userController.listUser();
            case USER_VIEW_BY_ID:
                return userController.viewUserById();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_UPDATE_BY_ID:
                return userController.updateUserById();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();
            case USER_REMOVE_BY_ID:
                return userController.removeUserById();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_SIGN_IN:
                return userController.signInUser();
            case USER_SIGN_OUT:
                return userController.signOutUser();
            case USER_VIEW_LOGGED:
                return userController.getCurrentUser();
            case USER_UPDATE_PASSWORD:
                return userController.updateUserPassword();
            case PROJECT_LIST_BY_CURRENT_USER:
                return userController.listProjectByCurrentUser();
            case TASK_LIST_BY_CURRENT_USER:
                return userController.listTaskByCurrentUser();

            case HISTORY_COMMAND:
                return viewCommandList();

            default: {
                commandList.poll();
                return systemController.displayError();
            }
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public int viewCommandList() {
        for (String pq : commandList) {
            System.out.println(pq);
        }
        return 0;
    }

}
